# Test task

A test task for internship in Dino Systems: a simple Spring Boot web-service with a single 'Hello world' web-page, deployed to a Heroku app.

Link: https://dino-systems-test.herokuapp.com/